import React, { useState } from "react";
//Unnecessary import statements
import { NavigationContainer } from "@react-navigation/native";
import AppContext from "./AppContext";
import BottomTabNavigator from "./BottomTabsNavigator";
import AppNavigator from "./AppNavigator";

/*
 * Unnecessary folders (qwerty) and files (AppContext.js) in project sturcture
 *
 * Try to avoid unnecessary dependencies in your project (package.json)
 * ex. cors is npt used anywhere
 */
const App = () => {
  // Need to cleanup the code to avoid declaration of unused variables
  const [spent, setSpent] = useState(0);

  // You can use plugins such as Prettier to autoformat code
  return <AppNavigator />;
};

export default App;
