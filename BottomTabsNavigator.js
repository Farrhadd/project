import React, { useState } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Content from './screens/Content';
import Expenses from './screens/Expenses';
import Market from './screens/Market';
import Message from './screens/Message';
import Profile from './screens/Profile';

const Tab = createBottomTabNavigator();

function BottomTabNavigator() {
  const [spent, setSpent] = useState(0);

  const handlePurchase = (amount) => {
    setSpent(prevSpent => prevSpent + parseFloat(amount));
  }

  return (
    <Tab.Navigator>
      <Tab.Screen 
        name="Market" 
        children={() => <Market onPurchase={handlePurchase} />} 
      />
      <Tab.Screen 
        name="Content" 
        component={Content} 
      />
      <Tab.Screen 
        name="Expenses" 
        children={() => <Expenses spent={spent} />} 
      />
      <Tab.Screen 
        name="Message" 
        component={Message} 
      />
      <Tab.Screen 
        name="Profile" 
        component={Profile} 
      />
    </Tab.Navigator>
  );
}

export default BottomTabNavigator;
