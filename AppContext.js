import React from "react";

//AppContext is not used anywhere, can be deleted
const AppContext = React.createContext();

export default AppContext;
