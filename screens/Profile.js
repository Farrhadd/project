import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Button,
  Image,
  TextInput,
  StyleSheet,
  Alert,
  ScrollView,
} from "react-native";
import * as ImagePicker from "expo-image-picker";

// Consider breaking component into smaller components.
function ProfileScreen() {
  /*
   * Consider using null or undefined. This can help in differentiating between 'not loaded' and 'loaded but empty' states.
   */
  const [userData, setUserData] = useState({});
  const [profilePicture, setProfilePicture] = useState(null);
  const [newContent, setNewContent] = useState("");
  const [viewPosts, setViewPosts] = useState(true);
  const [posts, setPosts] = useState([]);
  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    const fetchProfile = async () => {
      try {
        /*  Code should be concise and standartized, try either using async / await or promises
         * In other components you are using promises
         */
        const response = await fetch("http://localhost:3000/getProfile");
        const data = await response.json();
        setUserData(data);
        setProfilePicture(data.profilePicture);
        setPosts(data.posts || []);
        setPhotos(data.photos || []);
      } catch (error) {
        // Consider giving a user feedback
        console.error("Error fetching profile:", error);
      }
    };
    fetchProfile();
  }, []);

  const pickImage = async () => {
    // Code should be concise and standartized, try either using async / await or promises
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      if (viewPosts) {
        updateProfilePicture(result.uri);
      } else {
        addPhoto(result.uri);
      }
    }
  };

  const updateProfilePicture = async (uri) => {
    // ... same as your code
    // Didn't quite get it
  };

  const addPost = async () => {
    try {
      const response = await fetch("http://localhost:3000/addPost", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: userData.email,
          description: newContent,
        }),
      });
      if (response.ok) {
        setPosts([
          ...posts,
          { description: newContent, timestamp: Date.now() },
        ]);
        setNewContent("");
      }
    } catch (error) {
      console.error("Error adding post:", error);
    }
  };

  const addPhoto = async (uri) => {
    setPhotos([...photos, uri]);
  };

  const toggleView = () => {
    setViewPosts(!viewPosts);
  };

  const renderContent = () => {
    if (viewPosts) {
      return posts.map((post, index) => (
        <View key={index} style={styles.postContainer}>
          <Text>{post.description}</Text>
          <Text style={styles.timestamp}>
            {new Date(post.timestamp).toLocaleString()}
          </Text>
        </View>
      ));
    } else {
      return photos.map((photo, index) => (
        <Image key={index} source={{ uri: photo }} style={styles.photo} />
      ));
    }
  };

  return (
    <ScrollView style={styles.container}>
      <Image source={{ uri: profilePicture }} style={styles.profileImage} />
      <Button
        title={viewPosts ? "Update Profile Picture" : "Add Photo"}
        onPress={pickImage}
      />
      <Text style={styles.text}>Name: {userData.fullName}</Text>
      <Text style={styles.text}>Email: {userData.email}</Text>
      {viewPosts && (
        <>
          <TextInput
            placeholder="Add a new post..."
            value={newContent}
            onChangeText={setNewContent}
            style={styles.textInput}
          />
          <Button title="Add Post" onPress={addPost} />
        </>
      )}
      <Button
        title={viewPosts ? "Switch to Photos" : "Switch to Posts"}
        onPress={toggleView}
      />
      {renderContent()}
    </ScrollView>
  );
}

/*
 * Your styles are well-organized, but for larger projects, it's usually better to avoid inline styles.
 * Try to separate it into another file
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginBottom: 20,
    alignSelf: "center",
  },
  text: {
    fontSize: 16,
    marginBottom: 10,
    alignSelf: "center",
  },
  textInput: {
    height: 40,
    width: "80%",
    borderColor: "gray",
    borderWidth: 1,
    marginTop: 20,
    marginBottom: 20,
    paddingHorizontal: 10,
    alignSelf: "center",
  },
  postContainer: {
    marginVertical: 10,
    padding: 10,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 8,
  },
  timestamp: {
    marginTop: 5,
    fontSize: 12,
    color: "gray",
  },
  photo: {
    width: 100,
    height: 100,
    borderRadius: 8,
    margin: 10,
  },
});

export default ProfileScreen;
