import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Alert,
  TouchableOpacity,
  Text,
} from "react-native";
import axios from "axios";

const SignUp = ({ navigation }) => {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSignUp = () => {
    axios
      .post("http://localhost:3000/signup", { fullName, email, password })
      .then((response) => {
        Alert.alert("Success", "Registered successfully!");
        navigation.navigate("LogIn");
      })
      .catch((error) => {
        console.error("Registration Error:", error.response.data);
        Alert.alert("Error", "Registration failed!");
      });
  };

  return (
    <View style={styles.main}>
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder=" Name"
          onChangeText={setFullName}
          value={fullName}
        />
        <TextInput
          style={styles.input}
          placeholder=" Email"
          onChangeText={setEmail}
          value={email}
        />
        <TextInput
          style={styles.input}
          placeholder=" Password"
          onChangeText={setPassword}
          value={password}
          secureTextEntry={true}
        />
        <Button
          title="Go to Login"
          onPress={() => navigation.navigate("LogIn")}
        />
        <TouchableOpacity style={styles.buttonContainer} onPress={handleSignUp}>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

/*
 * Your styles are well-organized, but for larger projects, it's usually better to avoid inline styles.
 * Try to separate it into another file
 */

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "white",
  },
  buttonContainer: {
    marginTop: 20,
    backgroundColor: "#007BFF", // Button color
    padding: 12,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 16,
    paddingHorizontal: 32,
    gap: 12,
  },
  buttonText: {
    color: "white",
    textAlign: "center",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "500",
    letterSpacing: -0.25,
  },
  container: {
    borderRadius: 5,
    padding: 20,
    marginTop: "50%",
    marginLeft: 24,
    marginRight: 24,
    height: 182,
    backgroundColor: "white",
  },
  input: {
    marginLeft: 24,
    marginRight: 24,
    margin: 16,
    height: 50,
    borderRadius: 8,
    backgroundColor: "#F8FAFD",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "rgba(231, 236, 243, 1.0)",
    paddingLeft: 11,
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "400",
    letterSpacing: -0.25,
  },
});

export default SignUp;
