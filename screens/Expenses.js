import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, ScrollView, FlatList } from "react-native";
import axios from "axios";
import Svg, { Circle } from "react-native-svg";

/* As you are not using typescript it is good practice to use PropTypes */

const Expenses = () => {
  const [totalSpent, setTotalSpent] = useState(0);
  const [purchases, setPurchases] = useState([]);

  const fetchExpenses = async () => {
    try {
      //Use .env file for API url storing
      const response = await axios.get("http://localhost:3000/purchase");
      setPurchases(response.data);

      const spent = response.data.reduce(
        (acc, purchase) => acc + parseFloat(purchase.product.price),
        0
      );
      setTotalSpent(spent);
    } catch (error) {
      console.error("Error fetching purchases:", error);
    }
  };

  useEffect(() => {
    /*
     * Be cautious with setting intervals in useEffect, especially with a very short duration like 10ms.
     * This could lead to performance issues and unnecessary load on your server.
     */
    const interval = setInterval(() => {
      fetchExpenses();
    }, 10);

    return () => clearInterval(interval);
  }, []);

  /* Avoid magic numbers like the circle radius and stroke width.
   * declare them as constants at the top of your file or within a constants file.
   *
   * Try to extract into diffetent function
   */

  const percentageSpent = Math.min((totalSpent / 1000) * 100, 100).toFixed(2);
  const circleCircumference = 2 * Math.PI * 95;
  const strokeDashoffset =
    circleCircumference - (percentageSpent / 100) * circleCircumference;

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.insightsHeader}>Insights</Text>
      <View style={styles.circleContainer}>
        <Svg
          width="200"
          height="200"
          style={{ transform: [{ rotateZ: "-90deg" }] }}
        >
          <Circle
            cx="100"
            cy="100"
            r="95"
            strokeWidth="10"
            stroke="lightgray"
            fill="white"
          />
          <Circle
            cx="100"
            cy="100"
            r="95"
            strokeWidth="10"
            stroke="blue"
            fill="transparent"
            strokeDasharray={`${circleCircumference} ${circleCircumference}`}
            strokeDashoffset={strokeDashoffset}
          />
        </Svg>
        <View style={styles.circleTextContainer}>
          <Text style={styles.spentAmount}>{totalSpent.toFixed(2)}$</Text>
          <Text style={styles.percentageText}>{percentageSpent}% spent</Text>
        </View>
      </View>

      <Text style={styles.expensesHeader}>Expenses</Text>
      {/*  FlatList is generally discouraged because it can lead to performance issues during re-renders. */}
      <FlatList
        data={purchases}
        renderItem={({ item }) => (
          <View style={styles.purchaseRow}>
            <View style={styles.bluePoint} />
            <Text style={styles.productName}>{item.product.name}</Text>
            <Text style={styles.productPrice}>{item.product.price}$</Text>
          </View>
        )}
        keyExtractor={(item, index) => `purchase-${index}`}
      />
    </ScrollView>
  );
};

/*
 * Your styles are well-organized, but for larger projects, it's usually better to avoid inline styles.
 * Try to separate it into another file
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  insightsHeader: {
    fontSize: 24,
    marginBottom: 10,
    textAlign: "center",
  },
  circleContainer: {
    alignItems: "center",
    marginBottom: 30,
    position: "relative",
  },
  circleTextContainer: {
    position: "absolute",
    top: "35%",
    alignItems: "center",
  },
  spentAmount: {
    fontSize: 18,
  },
  percentageText: {
    fontSize: 14,
    color: "grey",
  },
  expensesHeader: {
    fontSize: 20,
    marginBottom: 10,
    textAlign: "center",
  },
  purchaseRow: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  bluePoint: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: "blue",
    marginRight: 10,
  },
  productName: {
    flex: 1,
  },
  productPrice: {
    marginLeft: 10,
  },
});

export default Expenses;
