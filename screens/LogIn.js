import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Alert,
  TouchableOpacity,
  Text,
} from "react-native";
import axios from "axios";

const LogIn = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogIn = () => {
    if (email.trim() === "" || password.trim() === "") {
      Alert.alert("Error", "Please enter both email and password!");
      return;
    }

    /*
     * Instead of hardcoding the API URL (http://localhost:3000/login), consider using environment variables.
     * Good for developing in different environtments such as development, testing, production
     *
     * For better readability and modern practice, consider using async/await syntax for API call.
     */
    axios
      .post("http://localhost:3000/login", { email, password })
      .then((response) => {
        Alert.alert("Success", "Logged in successfully!");
        navigation.navigate("Home");
      })
      .catch((error) => {
        /*
         * In the .catch block of your Axios call, consider logging or displaying the actual error message from the server
         * Easier debugging
         *
         * Try not to defined unused arguments like 'error'
         */
        Alert.alert("Error", "Login failed!");
      });
  };

  /*
   * You should define it outside the LogIn component or even in a separate file,
   * so it doesn't get redefined every time the LogIn component renders.
   */
  const CustomButton = ({ title, onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.button}>
      <Text style={styles.buttonText}>{title}</Text>
    </TouchableOpacity>
  );
  return (
    <View style={styles.main}>
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          onChangeText={setPassword}
          value={password}
          /*
           * For better understanding, use secureTextEntry={true}
           */
          secureTextEntry
        />
        <CustomButton title="Log In" onPress={handleLogIn} />
      </View>
    </View>
  );
};

/*
 * Your styles are well-organized, but for larger projects, it's usually better to avoid inline styles.
 * Try to separate it into another file
 */
const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "white",
  },
  container: {
    borderRadius: 5,
    padding: 20,
    marginTop: "50%",
    marginLeft: 24,
    marginRight: 24,
    height: 182,
    backgroundColor: "white",
  },
  input: {
    marginLeft: 24,
    marginRight: 24,
    margin: 16,
    height: 50,
    borderRadius: 8,
    backgroundColor: "#F8FAFD",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "rgba(231, 236, 243, 1.0)",
    paddingLeft: 11,
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "400",
    letterSpacing: -0.25,
  },
  button: {
    marginTop: 20,
    backgroundColor: "#007BFF",
    padding: 12,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 16,
    paddingHorizontal: 32,
    gap: 12,
  },
  buttonText: {
    color: "white",
    textAlign: "center",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "500",
    letterSpacing: -0.25,
  },
});

export default LogIn;
