import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from "react-native";

const IndividualMessage = ({ text, isIncoming }) => {
  return (
    <View
      style={isIncoming ? styles.incomingContainer : styles.outgoingContainer}
    >
      <Text style={isIncoming ? styles.incomingText : styles.outgoingText}>
        {text}
      </Text>
    </View>
  );
};

const Message = () => {
  const [messages, setMessages] = useState([
    {
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla quam eu faci lisis mollis.",
      isIncoming: true,
    },
    {
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      isIncoming: false,
    },
    {
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      isIncoming: true,
    },
  ]);
  const [currentText, setCurrentText] = useState("");

  const handleSend = () => {
    if (currentText.trim()) {
      setMessages([...messages, { text: currentText, isIncoming: false }]);
      setCurrentText("");
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.messagesContainer}>
        {messages.map((msg, index) => (
          <IndividualMessage
            key={index}
            text={msg.text}
            isIncoming={msg.isIncoming}
          />
        ))}
      </ScrollView>

      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          value={currentText}
          onChangeText={setCurrentText}
          placeholder="Message here..."
        />
        <TouchableOpacity style={styles.sendButton} onPress={handleSend}>
          <Text>Send</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

/*
 * Your styles are well-organized, but for larger projects, it's usually better to avoid inline styles.
 * Try to separate it into another file
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 10,
  },
  messagesContainer: {
    flexGrow: 1,
    justifyContent: "flex-end",
  },
  incomingContainer: {
    maxWidth: "70%",
    padding: 10,
    borderRadius: 10,
    marginVertical: 5,
    backgroundColor: "#e1e1e1",
    alignSelf: "flex-start",
  },
  outgoingContainer: {
    maxWidth: "70%",
    padding: 10,
    borderRadius: 10,
    marginVertical: 5,
    backgroundColor: "#4a90e2",
    alignSelf: "flex-end",
  },
  incomingText: {
    color: "#000",
  },
  outgoingText: {
    color: "#fff",
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
  },
  input: {
    flex: 1,
    padding: 10,
    borderRadius: 20,
    backgroundColor: "#e1e1e1",
  },
  sendButton: {
    marginLeft: 10,
    padding: 10,
    borderRadius: 20,
    backgroundColor: "#4a90e2",
  },
});

export default Message;
