import React, { useState } from "react";
import {
  View,
  Text,
  FlatList,
  Button,
  ScrollView,
  TextInput,
  Image,
  StyleSheet,
} from "react-native";
import axios from "axios";
/*
 * To improve readability you could break down the large Market
 * component into smaller components, such as ProductList, SearchBar, and ProductItem.
 */

// Would be good to extract into different file
const placeholderImage = "https://via.placeholder.com/150";

const allProducts = {
  hotDeals: Array(5)
    .fill()
    .map((_, i) => ({
      id: `hd${i}`,
      name: `Product HD ${i + 1}`,
      image: placeholderImage,
      price: (Math.random() * 100).toFixed(2),
    })),
  trending: Array(5)
    .fill()
    .map((_, i) => ({
      id: `tr${i}`,
      name: `Product TR ${i + 1}`,
      image: placeholderImage,
      price: (Math.random() * 100).toFixed(2),
    })),
  deals: Array(5)
    .fill()
    .map((_, i) => ({
      id: `de${i}`,
      name: `Product DE ${i + 1}`,
      image: placeholderImage,
      price: (Math.random() * 100).toFixed(2),
    })),
};

const Market = () => {
  const [searchQuery, setSearchQuery] = useState("");

  const handlePurchase = (product) => {
    axios
      .post("http://localhost:3000/purchase", { product })
      .then((response) => {
        console.log("Purchase recorded successfully!");
      })
      .catch((error) => {
        console.error("Error recording the purchase:", error);
      });
  };

  const filteredProducts = Object.values(allProducts)
    .flat()
    .filter((product) =>
      product.name.toLowerCase().includes(searchQuery.toLowerCase())
    );

  const renderProduct = ({ item }) => (
    <View style={styles.productContainer}>
      <Image source={{ uri: item.image }} style={styles.productImage} />
      <Text style={styles.productName}>{item.name}</Text>
      <Text style={styles.productPrice}>{item.price}$</Text>
      <View style={styles.buttonContainer}>
        {/*
         * The inline function in onPress={() => handlePurchase(item)} can be replaced with a
         * pre-defined function to avoid creating a new function on every render.
         */}
        <Button
          title="Buy"
          onPress={() => handlePurchase(item)}
          color="#00A1DE"
        />
      </View>
    </View>
  );

  return (
    <ScrollView style={styles.container}>
      <TextInput
        style={styles.searchBar}
        placeholder="Search..."
        onChangeText={(text) => setSearchQuery(text)}
        value={searchQuery}
      />
      {/*
       * Would be good to have debounce functionality, as user types. To limit number of requests to the backend
       */}
      {searchQuery ? (
        <FlatList
          data={filteredProducts}
          renderItem={renderProduct}
          keyExtractor={(item) => item.id}
          numColumns={3}
          showsHorizontalScrollIndicator={false}
        />
      ) : (
        <>
          <Text style={styles.sectionTitle}>Hot Deals</Text>
          <FlatList
            data={allProducts.hotDeals}
            renderItem={renderProduct}
            horizontal
            keyExtractor={(item) => item.id}
            showsHorizontalScrollIndicator={false}
          />

          <Text style={styles.sectionTitle}>Trending</Text>
          <FlatList
            data={allProducts.trending}
            renderItem={renderProduct}
            horizontal
            keyExtractor={(item) => item.id}
            showsHorizontalScrollIndicator={false}
          />

          <Text style={styles.sectionTitle}>Deals</Text>
          <FlatList
            data={allProducts.deals}
            renderItem={renderProduct}
            horizontal
            keyExtractor={(item) => item.id}
            showsHorizontalScrollIndicator={false}
          />
        </>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  searchBar: {
    marginBottom: 20,
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 10,
    paddingLeft: 20,
    fontSize: 18,
    height: 50,
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
  productContainer: {
    width: 130,
    marginRight: 5,
    alignItems: "center",
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
  },
  productImage: {
    width: 80,
    height: 80,
    marginBottom: 5,
  },
  productName: {
    textAlign: "center",
    fontSize: 12,
  },
  productPrice: {
    textAlign: "center",
    fontSize: 12,
    marginBottom: 5,
  },
  buttonContainer: {
    width: "100%",
  },
});

export default Market;
