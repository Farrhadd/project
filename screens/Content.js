import React, { useState, useEffect } from "react";
import { View, Text, Image, ScrollView, StyleSheet } from "react-native";

/*
 * Woud be better breaking down the component into smaller components.
 * For instance, you could have a Post and a Photo component. This makes the code more reusable and easier to manage.
 */

function Content() {
  const [userData, setUserData] = useState({ posts: [], photos: [] });

  useEffect(() => {
    const fetchProfile = async () => {
      try {
        // Try to use .env variables for API declaration
        const response = await fetch("http://localhost:3000/getProfile");
        const data = await response.json();
        setUserData(data);
      } catch (error) {
        /* When fetch fails due to network issues, it doesn't throw an error.
         * Instead, it sets response.ok to false. Consider checking response.ok and handling scenarios where it's false.
         */
        console.error("Error fetching profile:", error);
      }
    };

    fetchProfile();
  }, []);

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.header}>Posts</Text>
      {
        // Using index as a key in the .map() method is not recommended, especially if the list can change.

        userData.posts.map((post, index) => (
          <View key={index} style={styles.postContainer}>
            <Text>{post.description}</Text>
            <Text>{new Date(post.timestamp).toLocaleString()}</Text>
          </View>
        ))
      }

      <Text style={styles.header}>Photos</Text>
      {userData.photos.map((photo, index) => (
        <Image key={index} source={{ uri: photo }} style={styles.photo} />
      ))}
    </ScrollView>
  );
}

/*
 * Your styles are well-organized, but for larger projects, it's usually better to avoid inline styles.
 * Try to separate it into another file
 */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  header: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  postContainer: {
    marginBottom: 15,
    borderBottomColor: "gray",
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  photo: {
    width: "100%",
    height: 200,
    marginBottom: 15,
    resizeMode: "cover",
  },
});

export default Content;
